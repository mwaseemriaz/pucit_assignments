﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AuthenticationSystem
{
    public partial class users_grid : Form
    {
        public users_grid()
        {
            InitializeComponent();
            /*
                        SqlConnection con = new SqlConnection(config.connectionString);
                        con.Open();
                        string command = "select * from users ";
                        SqlCommand cmd = new SqlCommand(command, con);

                        SqlDataReader dr = cmd.ExecuteReader();
                        BindingSource bs = new BindingSource();
                        bs.DataSource = dr;
                        users_grid_view.DataSource = bs;
            */

            DataClasses1DataContext data = new DataClasses1DataContext();
            users_grid_view.DataSource = from row in data.users select row;

            users_grid_view.Columns[1].HeaderText = "Username";
            users_grid_view.Columns[2].HeaderText = "Password";
            users_grid_view.Columns[3].HeaderText = "First Name";
            users_grid_view.Columns[4].HeaderText = "Last Name";

//            con.Close();

        }

        private void users_grid_closed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void close_Click(object sender, EventArgs e)
        {

            authentication_form authenticationForm = new authentication_form();
            Close();
            authenticationForm.Show();

        }
    }
}
