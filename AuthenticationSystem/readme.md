This is an assignment to be shown in class on 19/7/2106 .
Assignment details are:

Create a screen with login and registration forms.
Register form will show after pressing a button.

On login check if username and password matches database record.
    if yes then show current users information in database
	if not then show an error message.
	
On registration form add following fields.
	first name, last name, username, password, repeat password
	on hitting register check if passwords match and username is not already present
	if validations pass then insert a new records in database and ask to login with new username.
	if validations fail then show an error message.
	
Note:
	The database files are stored in a zip archive in database folder. 
	unzip all files in there and update connection string in AuthenticationSystem\AuthenticationSystem\config.cs class file.
	
Good Luck	