﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace WebApplication1
{
    public partial class search_data : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void search_button_Click(object sender, EventArgs e)
        {

            string input1 = searchinput1.Text;

            SqlConnection con = new SqlConnection(config.connectionString);
            con.Open();

            string users_command = "select * from users WHERE first_name LIKE @searchInput1  OR last_name LIKE @searchInput1 ";
            SqlCommand users_cmd = new SqlCommand(users_command, con);

            users_cmd.Parameters.AddWithValue("@searchInput1", input1 + "%");

            SqlDataReader ur = users_cmd.ExecuteReader();
            GridView1.DataSource = ur;
            GridView1.DataBind();

            con.Close();


        }

    }
}