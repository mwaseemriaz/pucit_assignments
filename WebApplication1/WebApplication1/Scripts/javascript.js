﻿$(function () {

    if ($("#ServerSideErrors").html().length)
        $("#ServerSideErrorsContainer").show();

    // Set the active page
    var current_page = (document.location.pathname).substring(1);
    $('a[href="' + current_page + '"]').closest('li').addClass("active");

    $('body').on('submit', '.LoginForm', function () {

        var formErrors = false;

        hideErrors();

        var username = $("#username");
        var userpassword = $("#userpassword");

        if (!username.val())
            formErrors = displayError("This field is required", username)

        if (!userpassword.val())
            formErrors = displayError("This field is required", userpassword)


        if (formErrors)
            return false;

    })

    $('body').on('submit', '.RegisterForm', function () {

        var formErrors = false;

        hideErrors();

        var fname = $("#fname");
        var lname = $("#lname");

        var username = $("#r_username");
        var userpassword = $("#r_password");
        var repeat_password = $("#r_repeat_password");


        if (!fname.val())

            displayError("This field is required", fname)

        else
            if (fname.val().length < 3)

                formErrors = displayError("First Name should be atleast 3 characters", fname)

        if (!lname.val())

            formErrors = displayError("This field is required", lname)

        else
            if (lname.val().length < 3)

                formErrors = displayError("Last Name should be atleast 3 characters", lname)

        if (!username.val())

            formErrors = displayError("This field is required", username)

        else
            if (username.val().length < 6)

                formErrors = displayError("Username should be atleast 6 characters", username)


        if (!userpassword.val())

            formErrors = displayError("This field is required", userpassword)

        else
            if (userpassword.val().length < 6)
                formErrors = displayError("Password should be atleast 6 characters", userpassword)


        if (!repeat_password.val())

            formErrors = displayError("This field is required", repeat_password)

        else
            if (userpassword.val() != repeat_password.val())

                formErrors = displayError("Passwords does not match", repeat_password)


        if (formErrors)
            return false;

    })

    $('body').on('click', '.DismissError', function () {

        $(this).parent().remove();

    })

});

function hideErrors() {

    // remove all visible errors
    $('.error_container').remove();
    $('.ErrorField').removeClass('ErrorField');

    $("#ServerSideErrorsContainer").hide();
    $("#ServerSideErrors").html('');

}

function displayError(message, ErrorField) {

    var error_div = document.createElement('div');

    $(error_div).addClass('alert alert-danger alert-dismissible ');
    $(error_div).attr("role", "alert")
    $(error_div).append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(error_div).append(message);

    if (typeof ErrorField == 'undefined' || !ErrorField.length) {

        // this is general error
        $('.Form').preappend(error_div);

    } else {

        ErrorField.addClass('ErrorField');
        ErrorField.closest('.row').after(error_div);

    }


    return true;

}