﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace WebApplication1
{
    public partial class auth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime CurrentTimestamp = DateTime.Now;

        }

        protected void login_Click(object sender, EventArgs e)
        {
            Boolean formErrors = false;
            ServerSideErrors.Text = "";

            string user_name = username.Text;
            string user_pass = userpassword.Text;

            if (user_name == "" || user_pass == "")
            {
                formErrors = true;
                ServerSideErrors.Text += "Both fields are required. <br />";
            }

            if (!formErrors)
            {

                SqlConnection con = new SqlConnection(config.connectionString);
                con.Open();

                // LINQ:
                //  * Language Integrated Query
                //  * Shows syntax Error while coding

                string command = "SELECT * FROM users where username = @username and password = @password ";
                SqlCommand cmd = new SqlCommand(command, con);

                cmd.Parameters.AddWithValue("@username", user_name);
                cmd.Parameters.AddWithValue("@password", user_pass);

                SqlDataReader dr = cmd.ExecuteReader();


                if (dr.HasRows)
                {

//                    users_grid_view.Columns[1].HeaderText = "Username";
//                    users_grid_view.Columns[2].HeaderText = "Password";
//                    users_grid_view.Columns[3].HeaderText = "First Name";
//                    users_grid_view.Columns[4].HeaderText = "Last Name";

                    Response.Redirect("account.aspx");

                }
                else
                {
                    ServerSideErrors.Text += "Username or password is incorrect. <br />";
                    ServerSideErrors.Text += "Invalid Login details. <br />";
                }

                con.Close();

            }

        }

    }
}