﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace WebApplication1
{
    public partial class register : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void register_Click(object sender, EventArgs e)
        {

            // All fields are required
            string firstname = fname.Text;
            string lastname = lname.Text;
            string username = r_username.Text;
            string password = r_password.Text;
            string password2 = r_repeat_password.Text;

            Boolean form_errors = false;
            ServerSideErrors.Text = "";

            // Checking for empty column
            if (firstname == "" || lastname == "" || username == "" || password == "" || password2 == "")
            {
                ServerSideErrors.Text += "All fields are required. <br />";
                form_errors = true;
            }

            // Checking for passwords match
            if (password != password2)
            {
                ServerSideErrors.Text += "Passwords are not equal. <br />";
                form_errors = true;
            }


            SqlConnection con = new SqlConnection(config.connectionString);

            // checking for username duplication
            string checkSql = "SELECT username FROM users WHERE username = '{0}' ";
            checkSql = string.Format(checkSql, username);

            SqlCommand checkCommand = new SqlCommand(checkSql, con);
            checkCommand.Connection.Open();
            SqlDataReader checkReader = checkCommand.ExecuteReader();
            if (checkReader.HasRows)
            {
                ServerSideErrors.Text += "Username already exists please try a different one. <br />";

                form_errors = true;
            }
            checkCommand.Connection.Close();

            if (form_errors)
            {

            }
            else
            {
                // All information is correct insert record in database
                string insertSql = "INSERT INTO users (username, password, first_name, last_name) VALUES (@username, @password, @first_name, @last_name) ";

                SqlCommand insertCommand = new SqlCommand(insertSql, con);

                insertCommand.Parameters.AddWithValue("@username", username);
                insertCommand.Parameters.AddWithValue("@password", password);
                insertCommand.Parameters.AddWithValue("@first_name", firstname);
                insertCommand.Parameters.AddWithValue("@last_name", lastname);

                insertCommand.Connection.Open();
                insertCommand.ExecuteNonQuery();
                insertCommand.Connection.Close();

                // new record inserted simply hide registeration form
                Response.Redirect("auth.aspx");
            }
            
        
        }

    }
}